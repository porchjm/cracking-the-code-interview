from jpo_list import Payload, Node, SingleList

def remove_dups(list):
    if list.size < 2:
        return
    vals = {}

    cur = list.head
    while (cur.next != None):
        vals[cur.get_data()] = vals.get(cur.get_data(),0)+1
        cur = cur.next
    
    for key, val in vals.items():
        while (val > 1):
            print(f"Removing {key}")
            list.delete(key)
            val -= 1


sl = SingleList()
for n in range(10):
    if (n%3 == 0):
        sl.insertHead(n)
    sl.insertHead(n)

sl.insertHead(4)
print(f"Size: {sl.size}")
sl.print()

remove_dups(sl)
print(f"Size: {sl.size}")
sl.print()