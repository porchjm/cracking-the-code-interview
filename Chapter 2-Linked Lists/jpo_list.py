class Payload:
    def __init__(self, data = 0):
        self.data = data
    
    def Equals(self, data):
        return self.data == data
    
    def get(self):
        return self.data

class Node:
    def __init__(self, data, prev = None, next = None):
        self.data = Payload(data)
        self.prev = prev
        self.next = next
    
    def get_data(self):
        return self.data.get()

class SingleList:
    def __init__(self):
        self.head = None
        self.size = 0
    
    def insertHead(self, newNodeVal):
        newNode = Node(newNodeVal)
        newNode.next = self.head
        self.head = newNode
        self.size += 1

    def removeHead(self):
        if (self.head == None):
            return
        self.head = self.head.next
        self.size -= 1

    def search(self, data):
        cur = self.head

        while (cur != None):
            if cur.data.Equals(data):
                return cur
            cur = cur.next
        return None

    def delete(self, data):
        cur = self.head
        prev = None
        found = False
        
        while (cur != None):
            if (cur.data.Equals(data)):
                found = True
                break
            prev = cur
            cur = cur.next
        if (found == False):
            return False
        if (prev == None):
            self.removeHead()
            return True
        prev.next = cur.next
        self.size -= 1
        return True

    def print(self):
        cur = self.head
        out = ""

        while (cur != None):
            out += "{"+str(cur.get_data())+"}"
            cur = cur.next
        print(out)
